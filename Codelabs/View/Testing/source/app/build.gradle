apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-kapt'
apply plugin: "androidx.navigation.safeargs.kotlin"

android {
    namespace "com.example.android.architecture.blueprints.todoapp"

    defaultConfig {
        applicationId "com.example.android.architecture.blueprints.todoapp"
        minSdkVersion rootProject.minSdkVersion
        targetSdkVersion rootProject.targetSdkVersion
        compileSdk rootProject.compileSdkVersion
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }

    kotlin {
        jvmToolchain(17)
    }

    dataBinding {
        enabled = true
        enabledForTests = true
    }

    buildFeatures {
        buildConfig true
    }

    // Always show the result of every unit test when running via command line, even if it passes.
    testOptions.unitTests {
        includeAndroidResources = true
    }
}

dependencies {

    // App dependencies
    implementation "androidx.appcompat:appcompat:$appCompatVersion"
    implementation "androidx.swiperefreshlayout:swiperefreshlayout:$swipeRefreshLayoutVersion"
    implementation "com.google.android.material:material:$materialVersion"
    implementation "androidx.recyclerview:recyclerview:$recyclerViewVersion"
    implementation "androidx.annotation:annotation:$androidXAnnotations"
    implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion"
    implementation "com.jakewharton.timber:timber:$timberVersion"

    // Architecture Components
    implementation "androidx.room:room-runtime:$roomVersion"
    kapt "androidx.room:room-compiler:$roomVersion"
    implementation "androidx.room:room-ktx:$roomVersion"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$archLifecycleVersion"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$archLifecycleVersion"
    kapt "androidx.lifecycle:lifecycle-compiler:$archLifecycleVersion"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$archLifecycleVersion"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$archLifecycleVersion"
    implementation "androidx.navigation:navigation-fragment-ktx:$navigationVersion"
    implementation "androidx.navigation:navigation-ui-ktx:$navigationVersion"

    // Dependencies for local unit tests
    testImplementation "junit:junit:$junitVersion"
    testImplementation "androidx.test.ext:junit-ktx:1.1.3"
    testImplementation "androidx.test:core-ktx:1.4.0"
    testImplementation 'org.robolectric:robolectric:4.8'
    testImplementation "org.hamcrest:hamcrest-all:1.3"
    testImplementation "androidx.arch.core:core-testing:2.1.0"

    // AndroidX Test - Instrumented testing
    androidTestImplementation "androidx.test.ext:junit:$androidXTestExtKotlinRunnerVersion"
    androidTestImplementation "androidx.test.espresso:espresso-core:$espressoVersion"

    // Kotlin
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    implementation "androidx.fragment:fragment-ktx:$fragmentKtxVersion"
}
