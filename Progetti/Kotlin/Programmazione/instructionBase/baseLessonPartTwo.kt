package instructionBase

/**
 * @author Simone Tugnetti
 */

fun main() {

    /**
     * Funzioni
     * Esattamente come in java, ma con una sintassi simile a swifth
     * Le funzioni in kotlin hanno anche la possibilità di assegnare un valore a un parametro specifico
     * senza il bisogno di andare in ordine di assegnazione (al'esterno) oppure di salvare un dato di default
     * (all'interno)
     */
    fun isAllowedRide(age: Int): Boolean {
        return age >= 18
    }

    println("\nEsempio di funzione")

    if (isAllowedRide(30)) {
        println("Giovanni può guidare")
    } else {
        println("Giovanni non può guidare")
    }

    println("Esempio di Funzione con dati in parametro")

    fun isAllowedRideWithData(age: Int, name: String = "Paolo") {
        if (isAllowedRide(age)) {
            println("$name può guidare, avendo $age anni")
        } else {
            println("$name NON può guidare, avendo $age anni")
        }
    }

    isAllowedRideWithData(age = 17)

    /**
     * Vararg
     * Tipo di variabile utilizzato per identificare quando, all'interno di una funzione,
     * non vengono definiti specificatamente il numero di parametri a essa associata
     * Viene quindi creato un array d'informazioni
     */
    fun getNames(vararg names: String) {
        println("\nNumero di persone: ${names.indices.last+1}")
        println("Persone:")
        for (name in names) {
            print("$name, ")
        }
    }

    getNames("Giovanni", "Paolo")
    getNames("Giorgio", "Luca", "Gino")

    /**
     * Lambda Expression
     * Le lambda expression vengono utilizzate per poter eseguire operazioni complesse senza il bisogno di dover
     * creare una funzione che esegua tale operazione, potendole salvare all'interno di variabili che diventeranno
     * loro stesse delle funzioni
     */
    val lambdaVariable: (Int, String) -> String = {
            age: Int, name: String -> "$name ha $age anni"
    }

    println("\nEsempio di lambda expression in variabile")
    println(lambdaVariable(10 ,"Giorgio"))

    fun lambdaInFunction(items: Array<Int>, cl: (a: Int) -> Int): IntArray {
        val newList = IntArray(items.size)
        for ((i, item) in items.withIndex()) {
            newList[i] = cl(item)
        }
        return newList
    }

    val datoDaInserire: (Int) -> Int = {
            a: Int -> a*2
    }

    val listOfLambda = lambdaInFunction(arrayOf(10,20,30,40,50), datoDaInserire)

    println("\nEsempio di lambda Expression in Functions")
    for ((i, elem) in listOfLambda.withIndex()) {
        println("$i -> $elem")
    }

    // Lambda
    val isEven: (Int) -> Boolean = { it % 2 == 0 }

    // Lambda with receiver
    val isOdd: Int.() -> Boolean = { this % 2 == 1 }

    isEven(3)
    4.isOdd()

    /**
     * Nullable
     * Un tipo può essere (( nullable )) quando al suo interno può
     * contenere valore nullo oltre al valore specifico
     */
    val str1 = "Always not null"
    val s2: String? = null

    // Null in caso s2 == null
    println(s2?.length)

    /**
     * Elvis Operator
     * Ritorna il valore a sinistra nel caso la variabile non sia nulla,
     * quello a destra altrimenti
     */
    val length = s2?.length ?: 0

    s2?.let {
        println(it.length)
    }

    // s2 deve essere non nulla
    s2!!.length

    // Cast automatico non nullo
    if (s2 == null) return
    s2.length

    // I valori all'interno della lista possono essere nulli
    val list1: List<Int?>

    // La lista può essere nulla
    val list2: List<Int>?

    val any: Any = 9

    // Cast automatico della variabile Any se il contenuto è una Stringa
    if (any is String) {
        any.toUpperCase()
    }

    // Ritorna una varibile di tipo String nullable
    (any as? String)?.toUpperCase()

    val s3 = if (any is String) any else null

    val s4: String? = any as? String

}