package it.simonetugnetti.provarive

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import app.rive.runtime.kotlin.controllers.RiveFileController
import app.rive.runtime.kotlin.core.Alignment
import app.rive.runtime.kotlin.core.ExperimentalAssetLoader
import app.rive.runtime.kotlin.core.PlayableInstance
import app.rive.runtime.kotlin.core.RiveEvent
import app.rive.runtime.kotlin.core.RiveGeneralEvent
import app.rive.runtime.kotlin.core.RiveOpenURLEvent
import it.simonetugnetti.provarive.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        manageRiveAnimation()

    }

    @OptIn(ExperimentalAssetLoader::class)
    private fun manageRiveAnimation() {
        binding.riveAnimationView.setRiveResource(
            resId = R.raw.login_screen_character,
            stateMachineName = null
        )

        binding.riveAnimationView.registerListener(getRiveListener())
        binding.riveAnimationView.addEventListener(getRiveEventListener())
    }

    private fun getRiveListener() = object: RiveFileController.Listener {
        override fun notifyLoop(animation: PlayableInstance) {
            Log.i("RiveListener", "Loop")
        }

        override fun notifyPause(animation: PlayableInstance) {
            Log.i("RiveListener", "Pause")
        }

        override fun notifyPlay(animation: PlayableInstance) {
            Log.i("RiveListener", "Play")
        }

        override fun notifyStateChanged(stateMachineName: String, stateName: String) {
            Log.i("RiveListener", "State Changed")
        }

        override fun notifyStop(animation: PlayableInstance) {
            Log.i("RiveListener", "Stop")
        }

    }

    private fun getRiveEventListener() = object: RiveFileController.RiveEventListener {
        override fun notifyEvent(event: RiveEvent) {
            when (event) {
                is RiveOpenURLEvent -> {
                    Log.i("RiveEvent", "Open URL Rive event: ${event.url}")
                }
                is RiveGeneralEvent -> {
                    Log.i("RiveEvent", "General Rive event")
                }
            }
            Log.i("RiveEvent", "name: ${event.name}")
            Log.i("RiveEvent", "type: ${event.type}")
            Log.i("RiveEvent", "properties: ${event.properties}")
            // `data` contains all information in the event
            Log.i("RiveEvent", "data: ${event.data}");
        }
    }

}