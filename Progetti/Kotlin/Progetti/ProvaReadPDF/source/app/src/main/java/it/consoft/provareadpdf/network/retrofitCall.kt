package it.consoft.provareadpdf.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Streaming

private const val BASEURL = "http://www.africau.edu/images/default/"

private val retrofitBuild = Retrofit.Builder().baseUrl(BASEURL).build()

interface RetrofitCall {
    @GET("sample.pdf")
    @Streaming
    fun downloadFile(): Call<ResponseBody>
}

object RetrofitObj {
    val retrofitService = retrofitBuild.create(RetrofitCall::class.java)
}