package it.consoft.provaauthsystempin

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import it.consoft.provaauthsystempin.databinding.ActivityBiometricBinding

// https://www.raywenderlich.com/18782293-android-biometric-api-getting-started

class BiometricActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBiometricBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBiometricBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val callbackAuth = object: BiometricAuthListener {
            override fun onBiometricAuthError(errorCode: Int, errorString: CharSequence) {
                binding.titleBiometric.text = "Autenticazione fallita"
            }

            override fun onBiometricAuthSuccess(result: BiometricPrompt.AuthenticationResult) {
                binding.titleBiometric.text = "Autenticazione effettuata"
            }

        }

        showBiometricStrongPrompt(listener = callbackAuth)

    }

    private fun hasBiometricCapability(): Int {
        val biometricManager = BiometricManager.from(this)
        return biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK)
    }

    private fun isBiometricReady() = hasBiometricCapability() == BiometricManager.BIOMETRIC_SUCCESS

    private fun setBiometricPromptInfo(title: String, description: String): BiometricPrompt.PromptInfo {
        val builder = BiometricPrompt.PromptInfo.Builder().setTitle(title).setDescription(description)

        builder.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
                if (isBiometricReady()) {
                    setNegativeButtonText("Annulla")
                    setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_WEAK)
                } else setAllowedAuthenticators(BiometricManager.Authenticators.DEVICE_CREDENTIAL)
            else setDeviceCredentialAllowed(true)
        }

        return builder.build()
    }

    private fun initBiometricPrompt(listener: BiometricAuthListener): BiometricPrompt {
        val executor = ContextCompat.getMainExecutor(this)

        val callback = object: BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                listener.onBiometricAuthError(errorCode, errString)
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Log.e(this.javaClass.simpleName, "AuthFailed")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                listener.onBiometricAuthSuccess(result)
            }
        }

        return BiometricPrompt(this, executor, callback)

    }

    private fun showBiometricStrongPrompt(
        title: String = "Autenticazione",
        description: String = "Inserisci la tua impronta, FaceID o PIN per procedere",
        listener: BiometricAuthListener) {

        val promptInfo = setBiometricPromptInfo(title, description)

        val biometricPrompt = initBiometricPrompt(listener)

        biometricPrompt.authenticate(promptInfo)

    }

}
