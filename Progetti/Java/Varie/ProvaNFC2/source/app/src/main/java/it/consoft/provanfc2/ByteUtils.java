package it.consoft.provanfc2;

public class ByteUtils {

    public static boolean isNullOrEmpty(byte[] array) {
        if (array == null) return true;
        for (byte b : array) if (((int) b) != 0) return false;
        return true;
    }

}
